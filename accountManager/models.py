from django.contrib.auth.models import User
from django.db import models


# Create your models here.


class Account(models.Model):
    phone_number = models.IntegerField(blank=False, null=False)
    rank = models.IntegerField(blank=False, null=False)
    city = models.CharField(max_length=100, blank=False, null=False)
    day = models.CharField(max_length=10, blank=False, null=False)
    user = models.ForeignKey(User, on_delete=models.SET_DEFAULT, default=None, null=True, related_name='accounts')

    def send_success_email(self):
        from django.core.mail import send_mail
        from HamayeshVoroodi.settings import EMAIL_HOST_USER

        name = self.user.first_name + ' ' + self.user.last_name
        day = 'پنجشنبه'
        if self.day == 'j':
            day = 'جمعه'

        message = '''
            با عرض سلام.
            جناب خانم/آقای {} شما با موفقیت در سیزدهمین همایش تخصصی معرفی رشته های فنی، مهندسی و علوم پایه دانشگاه صنعتی شریف، در روز {} ثبت نام شدید.
            
            
            جهت اطلاع از جزئیات رویداد کانال ما را دنبال کنید.
            http://t.me/sut_1399
            '''.format(name, day)

        send_mail(
            'ثبت نام در همایش معرفی رشته شریف',
            message,
            EMAIL_HOST_USER,
            [self.user.email],
            fail_silently=False,
        )
