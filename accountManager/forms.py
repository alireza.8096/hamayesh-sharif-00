from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from accountManager.models import Account


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['phone_number', 'rank', 'city', 'day']


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', "email", "username"]